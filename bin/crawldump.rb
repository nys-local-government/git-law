#!/usr/bin/ruby

require 'net/http'
require 'rubygems'
require 'nokogiri'
require 'open-uri'

def make_file_name(string)
  string.downcase.gsub(/[^a-zA-Z0-9]/,'-').squeeze('-').gsub(/^-/,'').
  gsub(/-$/,'') + ".md"
end

URL_ROOT = "http://ecode360.com/"

def crawl_toc(uri,summary,level=0)
  page = Nokogiri::HTML( open( uri ) )
  page.css("#toc a.titleLink").each do |link|
    level = crawl( page, link, summary, level )
  end
  level
end 

def crawl(page,link,summary,level)
  number = link.css('.titleNumber').first.text
  title = link.css('.titleTitle').first.text
  title_string = "#{number} #{title}"
  index = summary.index { |entry| entry[:title] == title_string }
  level = summary[index][:level] if index
  unless index
    summary << { level: level, title: title_string, title_string: title_string }
    index = summary.length - 1
  end
  if link["href"][0] == '#'
    summary[index][:title_string] = "[#{title_string}](#{make_file_name(title_string)})"
    File.open("db/dump/#{make_file_name(title_string)}",'w') do |file|
      file << "#{'#' * level}# #{title_string}\n\n"
      file << page.css("#{link['href']}_content").text
    end
    level
  else
    crawl_toc(URL_ROOT+link["href"],summary,level+1)
  end
  level
end

summary = []
crawl_toc "#{URL_ROOT}/TO1768", summary

File.open("db/dump/SUMMARY.md",'w') do |summary_file|
  summary.each do |entry|
    summary_file << "#{' ' * entry[:level]}* #{entry[:title_string]}\n"
  end
end

#root = "http://ecode360.com/"
#page.css('a.titleLink').each do |title_link|
#  title_number = title_link.css('.titleNumber').first.text
#  title_title = title_link.css('.titleTitle').first.text
#  title_string = "#{title_number} #{title_title}"
#  summary_file << "* [#{title_string}](#{make_file_name(title_string)})\n"
#  title = Nokogiri::HTML(open(root + title_link['href']))
#  title_file = File.new("db/dump/#{make_file_name(title_string)}.md",'w')
#  title.css('#toc a.titleLink').each do |section_link|
#    section_number = section_link.css('.titleNumber').first.text
#    section_title = section_link.css('.titleTitle').first.text
#    section_id = section_link['href']
#    if section_id[0] == '/'
#      crawl(root + section_id,summary_file,level+1)
#    else
#      title_file << "#{section_number} #{section_title}"
#      section_content = title.css("#{section_id}_content").text
#      title_file << section_content
#      title_file << "\n"
#  end
#  title_file.close
#end

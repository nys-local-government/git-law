#= require ace-builds/src-min-noconflict/ace.js
#= require angular
#= require angular-animate
#= require angular-sanitize/angular-sanitize.min.js
#= require angular-ui-router
#= require angular-resource/angular-resource
#= require ng-file-upload/angular-file-upload-shim.js
#= require ng-file-upload/angular-file-upload.js
#= require angular-rails4-templates
#= require angular-ui-utils/ui-utils.min.js
#= require angular-bootstrap/ui-bootstrap-tpls.js
#= require angular-ui-select/dist/select.min.js
#= require angular-ui-tree/dist/angular-ui-tree.js
#= require angular-ui-ace/ui-ace.js
#= require_tree ./ng-app
#= require_tree ./templates
